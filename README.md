# Celestial
- A [Weave](https://github.com/Weave-MC) mod for improving LunarClient experience.
- Check out the old version [here](https://github.com/Monmcgt/Lunar-Agent). It is a pure java agent (not a Weave mod).

<br>

## Features
**Bedwars Stats**  
![Bedwars Stats](assets/bedwars-stats.jpg)  
**Show stats when hovering over a player's chat message.**   
![Hover Stats](assets/hover-stats.jpg)  
**Keybinds Settings**  
![Keybinds Settings](assets/control-gui.jpg)  
**Settings GUI**  
![Settings GUI](assets/settings-gui-hypixel.jpg)  

### Hypixel Bedwars Stats Checker
#### Specify: `/celestial bedwars specify <player>`
#### Who (/who): `/celestial bedwars who`
#### Party (/party list): `/celestial bedwars party`

<br>

## How to use
1. [Download](#download) Celestial mod.
2. Place the jar in your Weave mods folder.
    1. Windows: `%userprofile%\.lunarclient\mods`
    2. Unix: `~/.lunarclient/mods`
3. Add [Weave Loader](https://github.com/Weave-MC/Weave-Loader) to your java agent list.

<br>

## Download
- [Releases](https://gitlab.com/Candicey/celestial/-/releases)

<br>

## Build
- Clone the repository.
- Run `./gradlew build` in the root directory.
- The built jar file will be in `build/libs`.

<br>

## Credits
- [Weave MC](https://github.com/Weave-MC) - For the mod loader.

<br>

## License
- Celestial is licensed under the [GNU General Public License Version 3](https://gitlab.com/Candicey/celestial/-/blob/master/LICENSE).
- (Actually, I want to use [Creative Commons Non-Commercial](https://creativecommons.org/licenses/by-nc/4.0/). But the Weave mod loader is licensed under the GPL 3.0.)
