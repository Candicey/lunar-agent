import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version ("1.8.0")
    id("com.github.weave-mc.weave") version ("8b70bcc707")
}

group = "com.gitlab.candicey.celestial"
version = "1.0.0"

minecraft.version("1.8.9")

repositories {
    mavenCentral()
    maven("https://jitpack.io")
    maven("https://repo.spongepowered.org/maven")
}

dependencies {
    testImplementation(kotlin("test"))

//    implementation("org.l33tlabs.twl:pngdecoder:1.0")
    implementation("com.google.code.gson:gson:2.10.1")

    compileOnly("com.github.Weave-MC:Weave-Loader:94d4e893")
    compileOnly("org.spongepowered:mixin:0.8.5")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}

tasks.jar {
    val wantedJar = listOf<String>()
    configurations["compileClasspath"]
        .filter { wantedJar.find { wantedJarName -> it.name.contains(wantedJarName) } != null }
        .forEach { file: File ->
            from(zipTree(file.absoluteFile)) {
                this.duplicatesStrategy = DuplicatesStrategy.EXCLUDE
            }
        }
}