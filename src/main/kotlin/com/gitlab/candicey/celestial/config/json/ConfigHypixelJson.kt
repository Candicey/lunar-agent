package com.gitlab.candicey.celestial.config.json

data class ConfigHypixelJson(
    val chatStatsCheckerJson: ChatStatsCheckerJson = ChatStatsCheckerJson(),
) {
    data class ChatStatsCheckerJson(
        var enabled: Boolean = true,
    )
}