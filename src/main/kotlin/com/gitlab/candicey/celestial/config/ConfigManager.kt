package com.gitlab.candicey.celestial.config

import com.gitlab.candicey.celestial.GSON
import com.gitlab.candicey.celestial.GSON_PRETTY
import com.gitlab.candicey.celestial.config.json.ConfigApiJson
import com.gitlab.candicey.celestial.config.json.ConfigHypixelJson
import java.io.BufferedWriter
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.*

object ConfigManager {
    private const val configApiDataFileName = "api.json"
    private lateinit var configApiData: ConfigApiJson

    private const val configHypixelDataFileName = "hypixel.json"
    private lateinit var configHypixelData: ConfigHypixelJson

    fun init() {
        val configApiFile = getConfigPath(configApiDataFileName)
        val configHypixelFile = getConfigPath(configHypixelDataFileName)
        configApiData = GSON.fromJson(configApiFile.bufferedReader(), ConfigApiJson::class.java)
        configHypixelData = GSON.fromJson(configHypixelFile.bufferedReader(), ConfigHypixelJson::class.java)
    }

    fun save() {
        val configApiFile = getConfigPath(configApiDataFileName)
        val configHypixelFile = getConfigPath(configHypixelDataFileName)
        configApiFile.bufferedWriter().use { it.write(GSON_PRETTY.toJson(configApiData)) }
        configHypixelFile.bufferedWriter().use { it.write(GSON_PRETTY.toJson(configHypixelData)) }
    }

    private fun getConfigFolder(): Path {
        val dir = Paths.get(System.getProperty("user.home"), ".lunarclient", "Celestial")
        if (dir.exists() && !dir.isDirectory()) Files.delete(dir)
        if (!dir.exists()) dir.createDirectory()
        return dir
    }

    private fun getConfigPath(fileName: String): Path = getConfigFolder().resolve(fileName).also { if (!it.exists()) it.createFile().bufferedWriter().use { bufferedWriter: BufferedWriter -> bufferedWriter.write("{}") } }

    fun getConfigApiData(): ConfigApiJson = configApiData
    fun getConfigHypixelData(): ConfigHypixelJson = configHypixelData
}