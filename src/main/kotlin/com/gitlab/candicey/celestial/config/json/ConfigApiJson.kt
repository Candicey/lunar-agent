package com.gitlab.candicey.celestial.config.json

data class ConfigApiJson(
    var hypixelApiKey: String = "",
)