package com.gitlab.candicey.celestial.wrapper

import com.google.common.collect.Ordering
import net.minecraft.client.gui.GuiPlayerTabOverlay
import net.minecraft.client.network.NetworkPlayerInfo
import java.lang.reflect.Field

object GuiPlayerTabOverlayWrapper {
    private val entityOrderingField: Field =
        GuiPlayerTabOverlay::class.java.getField("field_175252_a").also { it.isAccessible = true }
    @Suppress("UNCHECKED_CAST")
    var entityOrdering: Ordering<NetworkPlayerInfo>
        get() = entityOrderingField.get(null) as Ordering<NetworkPlayerInfo>
        set(value) = entityOrderingField.set(null, value)
}