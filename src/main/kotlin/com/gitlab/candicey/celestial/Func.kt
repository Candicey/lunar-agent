package com.gitlab.candicey.celestial

fun info(message: String) = LOGGER.info("[Celestial] $message")