package com.gitlab.candicey.celestial.command.impl.debug

import com.gitlab.candicey.celestial.command.CommandAbstract
import com.gitlab.candicey.celestial.command.CommandInfo
import com.gitlab.candicey.celestial.extension.addChatMessage
import com.gitlab.candicey.celestial.extension.toChatComponent
import com.gitlab.candicey.celestial.util.EntityUtil

@CommandInfo("lookingentity", "le")
object LookingEntityDebugCommand : CommandAbstract() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: Array<String>) {
        val lookingEntity = EntityUtil.getLookingEntity()
        if (lookingEntity != null) {
            "Looking entity: ${lookingEntity.name}".toChatComponent().addChatMessage()
        } else {
            "No entity found".toChatComponent().addChatMessage()
        }
    }
}