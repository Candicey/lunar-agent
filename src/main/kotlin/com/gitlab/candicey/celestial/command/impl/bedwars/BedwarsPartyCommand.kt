package com.gitlab.candicey.celestial.command.impl.bedwars

import com.gitlab.candicey.celestial.command.CommandAbstract
import com.gitlab.candicey.celestial.command.CommandInfo
import com.gitlab.candicey.celestial.command.impl.BedwarsCommand
import com.gitlab.candicey.celestial.extension.addCLTPrefix
import com.gitlab.candicey.celestial.extension.addChatMessage
import com.gitlab.candicey.celestial.extension.toChatComponent
import com.gitlab.candicey.celestial.helper.BedwarsStatsHelper
import com.gitlab.candicey.celestial.task.HypixelTask
import com.gitlab.candicey.celestial.util.red

@CommandInfo("party", "p")
object BedwarsPartyCommand : CommandAbstract() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: Array<String>) {
        if (BedwarsStatsHelper.invalidHypixelApiKey) {
            BedwarsStatsHelper.printInvalidApiKeyMessage()
            return
        }

        HypixelTask.party({ (isInParty: Boolean, players: List<String>) ->
            if (isInParty) {
                players.forEach { player ->
                    BedwarsCommand.executors.submit {
                        BedwarsStatsHelper.checkPlayerStats(player)
                    }
                }
            } else {
                "${red}You are not in a party!".addCLTPrefix().toChatComponent().addChatMessage()
            }
        })
    }
}