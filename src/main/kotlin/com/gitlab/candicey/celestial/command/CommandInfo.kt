package com.gitlab.candicey.celestial.command

annotation class CommandInfo(
    vararg val aliases: String,
)
