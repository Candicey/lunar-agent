package com.gitlab.candicey.celestial.command.impl.bedwars

import com.gitlab.candicey.celestial.command.CommandAbstract
import com.gitlab.candicey.celestial.command.CommandInfo
import com.gitlab.candicey.celestial.command.impl.BedwarsCommand
import com.gitlab.candicey.celestial.helper.BedwarsStatsHelper
import com.gitlab.candicey.celestial.task.HypixelTask

@CommandInfo("who", "w")
object BedwarsWhoCommand : CommandAbstract() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: Array<String>) {
        if (BedwarsStatsHelper.invalidHypixelApiKey) {
            BedwarsStatsHelper.printInvalidApiKeyMessage()
            return
        }

        HypixelTask.who({ playerList ->
            playerList.forEach { player ->
                BedwarsCommand.executors.submit {
                    BedwarsStatsHelper.checkPlayerStats(player)
                }
            }
        })
    }
}