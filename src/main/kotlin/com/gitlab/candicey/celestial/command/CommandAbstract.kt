package com.gitlab.candicey.celestial.command

import com.gitlab.candicey.celestial.COMMAND_PREFIX
import com.gitlab.candicey.celestial.PREFIX
import com.gitlab.candicey.celestial.extension.addCLTPrefix
import com.gitlab.candicey.celestial.extension.addChatMessage
import com.gitlab.candicey.celestial.extension.toChatComponent
import com.gitlab.candicey.celestial.util.*

abstract class CommandAbstract {
    var runSubCommand = false
    var runPostExecute = false
    var runPreSubCommand = false
    open val subCommands: MutableList<CommandAbstract> = mutableListOf()

    var commandInfo: CommandInfo
    var hideCommand = false

    var aliases: Array<String>

    init {
        commandInfo = javaClass.getAnnotation(CommandInfo::class.java) ?: throw IllegalArgumentException("Command class must be annotated with @CommandInfo (class: ${javaClass.name})")
        hideCommand = javaClass.getAnnotation(HideCommand::class.java) != null
        aliases = commandInfo.aliases.toList().toTypedArray()
    }

    fun preExecute(args: List<String>, rawMessage: String, previousCommandName: Array<String>) {
        runSubCommand = false
        runPostExecute = false
        runPreSubCommand = false

        execute(args, rawMessage, previousCommandName)

        if (runSubCommand) {
            val nextArgsArray = args.toTypedArray().copyOfRange(1, args.size)
            val nextCommandName = args[0]

            val filter = subCommands
                .filter { it.isThisCommand(nextCommandName) }
                .toMutableList()

            if (filter.isEmpty()) {
                "$red${bold}Command not found!".addCLTPrefix().toChatComponent().addChatMessage()
            } else {
                if (runPreSubCommand) {
                    preRunSubCommand(filter)
                }
                filter.forEach {
                    it.preExecute(nextArgsArray.toList(), rawMessage, previousCommandName.plus(aliases[0]))
                }
            }

            if (runPostExecute) {
                postExecute()
            }
        }
    }

    open fun execute(args: List<String>, rawMessage: String, previousCommandName: Array<String>) {
        if (args.isEmpty()) {
            sendHelpMessage(previousCommandName)
        } else {
            runSubCommand = true
        }
    }

    open fun postExecute() {
    }

    open fun preRunSubCommand(subCommands: MutableList<CommandAbstract>) {
    }

    fun isThisCommand(command: String): Boolean {
        aliases.forEach {
            if (it.equals(command, true)) {
                return true
            }
        }
        return false
    }

    open fun sendHelpMessage(previousCommandName: Array<String>) {
        "$yellow${bold}${underline}Available commands$yellow${bold}:".addCLTPrefix().toChatComponent().addChatMessage()
        subCommands.forEach { commandAbstract ->
            val commandPrefix = COMMAND_PREFIX[0]
            val alias = commandAbstract.aliases[0]
            if (!commandAbstract::class.java.annotations.map { it::class.java }.contains(HideCommand::class.java)) {
                " $grey$bold$rightArrow$reset $dark_aqua$PREFIX$commandPrefix ${previousCommandName.plus(alias).joinToString(" ")}".addCLTPrefix().toChatComponent().addChatMessage()
            }
        }
    }
}