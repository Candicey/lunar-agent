package com.gitlab.candicey.celestial.command.impl.debug

import club.maxstats.weave.loader.api.event.ChatReceivedEvent
import club.maxstats.weave.loader.api.event.EventBus
import com.gitlab.candicey.celestial.command.CommandAbstract
import com.gitlab.candicey.celestial.command.CommandInfo
import net.minecraft.util.IChatComponent

@CommandInfo("s2c")
object S2CDebugCommand : CommandAbstract() {
    private var enable = false
    private val handler = handler@ { event: ChatReceivedEvent ->
        if (!enable) return@handler

        println("event.message.unformattedText = ${event.message.unformattedText}")
        println("event.message.formattedText = ${event.message.formattedText}")
        println(
            "IChatComponent.Serializer.componentToJson(event.message) = ${
                IChatComponent.Serializer.componentToJson(
                    event.message
                )
            }"
        )
    }

    init {
        EventBus.subscribe(handler)
    }

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: Array<String>) {
        enable = !enable
    }
}