package com.gitlab.candicey.celestial.command.impl.bedwars

import com.gitlab.candicey.celestial.command.CommandAbstract
import com.gitlab.candicey.celestial.command.CommandInfo
import com.gitlab.candicey.celestial.command.impl.BedwarsCommand
import com.gitlab.candicey.celestial.extension.addCLTPrefix
import com.gitlab.candicey.celestial.extension.addChatMessage
import com.gitlab.candicey.celestial.extension.isValidMinecraftUsername
import com.gitlab.candicey.celestial.extension.toChatComponent
import com.gitlab.candicey.celestial.helper.BedwarsStatsHelper
import com.gitlab.candicey.celestial.util.red

@CommandInfo("specify", "specific", "s")
object BedwarsSpecifyCommand : CommandAbstract() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: Array<String>) {
        if (args.isEmpty()) {
            "${red}Please specify a player!".addCLTPrefix().toChatComponent().addChatMessage()
            return
        }

        if (BedwarsStatsHelper.invalidHypixelApiKey) {
            BedwarsStatsHelper.printInvalidApiKeyMessage()
            return
        }

        args.forEach { player ->
            if (player.isValidMinecraftUsername()) {
                BedwarsCommand.executors.submit {
                    BedwarsStatsHelper.checkPlayerStats(player)
                }
            } else {
                "$player is not a valid Minecraft username!".addCLTPrefix().toChatComponent().addChatMessage()
            }
        }
    }
}