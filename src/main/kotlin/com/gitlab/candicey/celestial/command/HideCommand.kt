package com.gitlab.candicey.celestial.command

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class HideCommand
