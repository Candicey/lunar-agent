package com.gitlab.candicey.celestial.command.impl.debug

import com.gitlab.candicey.celestial.botList
import com.gitlab.candicey.celestial.command.CommandAbstract
import com.gitlab.candicey.celestial.command.CommandInfo
import com.gitlab.candicey.celestial.extension.addChatMessage
import com.gitlab.candicey.celestial.extension.toChatComponent
import com.gitlab.candicey.celestial.util.PlayerUtil
import com.gitlab.candicey.celestial.util.lime
import com.gitlab.candicey.celestial.util.red
import com.gitlab.candicey.celestial.util.yellow

@CommandInfo("playerlist", "pl")
object PlayerListDebugCommand : CommandAbstract() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: Array<String>) {
        val tabPlayerList = PlayerUtil.getTabPlayerList()

        "---------------------".toChatComponent().addChatMessage()
        tabPlayerList.joinToString(", ") {
            "${if (botList.contains(it)) red else lime}name: ${it.name}$yellow"
        }.toChatComponent().addChatMessage()
        "---------------------".toChatComponent().addChatMessage()
    }
}