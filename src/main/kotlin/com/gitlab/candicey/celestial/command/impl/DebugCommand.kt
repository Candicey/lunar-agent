package com.gitlab.candicey.celestial.command.impl

import com.gitlab.candicey.celestial.command.HideCommand
import com.gitlab.candicey.celestial.command.CommandAbstract
import com.gitlab.candicey.celestial.command.CommandInfo
import com.gitlab.candicey.celestial.command.impl.debug.LookingEntityDebugCommand
import com.gitlab.candicey.celestial.command.impl.debug.PlayerListDebugCommand
import com.gitlab.candicey.celestial.command.impl.debug.S2CDebugCommand

@HideCommand
@CommandInfo("debug")
object DebugCommand : CommandAbstract() {
    override val subCommands: MutableList<CommandAbstract>
        get() = mutableListOf(
            S2CDebugCommand,
            LookingEntityDebugCommand,
            PlayerListDebugCommand,
        )
}