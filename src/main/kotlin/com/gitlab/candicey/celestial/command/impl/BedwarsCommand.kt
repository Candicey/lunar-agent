package com.gitlab.candicey.celestial.command.impl

import com.gitlab.candicey.celestial.command.CommandAbstract
import com.gitlab.candicey.celestial.command.CommandInfo
import com.gitlab.candicey.celestial.command.impl.bedwars.BedwarsPartyCommand
import com.gitlab.candicey.celestial.command.impl.bedwars.BedwarsSpecifyCommand
import com.gitlab.candicey.celestial.command.impl.bedwars.BedwarsWhoCommand
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@CommandInfo("bedwars", "bedwar", "bw")
object BedwarsCommand : CommandAbstract() {
    override val subCommands: MutableList<CommandAbstract>
        get() = mutableListOf(
            BedwarsSpecifyCommand,
            BedwarsWhoCommand,
            BedwarsPartyCommand,
//            BedwarsListCommand,
        )

    val executors: ExecutorService = Executors.newFixedThreadPool(30)
}