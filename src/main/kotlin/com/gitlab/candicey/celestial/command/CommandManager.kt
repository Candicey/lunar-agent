package com.gitlab.candicey.celestial.command

import club.maxstats.weave.loader.api.event.ChatSentEvent
import club.maxstats.weave.loader.api.event.EventBus
import com.gitlab.candicey.celestial.COMMAND_PREFIX
import com.gitlab.candicey.celestial.PREFIX
import com.gitlab.candicey.celestial.command.impl.BedwarsCommand
import com.gitlab.candicey.celestial.command.impl.DebugCommand
import com.gitlab.candicey.celestial.extension.addCLTPrefix
import com.gitlab.candicey.celestial.extension.addChatMessage
import com.gitlab.candicey.celestial.extension.toChatComponent
import com.gitlab.candicey.celestial.util.*
import java.util.concurrent.Executors
import java.util.function.Consumer

private val commandsExecutor = Executors.newFixedThreadPool(150)

private val commands = mutableListOf<CommandAbstract>()

object CommandManager {
    private val chatConsumer: Consumer<ChatSentEvent> = Consumer {
        if (onMessage(it.message)) {
            it.cancelled = true
        }
    }

    private fun onMessage(raw: String): Boolean {
        if (!raw.startsWith(PREFIX)) {
            return false
        }

        val message = raw.substring(PREFIX.length)
        val split = message.split("\\s+".toRegex())

        if (split.isEmpty()) {
            return false
        }

        var equal = false
        COMMAND_PREFIX.forEach {
            if (split[0].equals(it, true)) {
                equal = true
            }
        }
        if (!equal) {
            return false
        }

        if (split.size < 2 || split[1].equals("help", true)) {
            sendHelpMessage()
            return true
        }

        val args = split.drop(2)

        val commands = commands.filter { it.isThisCommand(split[1]) }

        if (commands.isEmpty()) {
            "${red}${bold}Command not found!".addCLTPrefix().toChatComponent().addChatMessage()
        } else {
            commandsExecutor.submit {
                commands.forEach {
                    it.preExecute(args, raw, emptyArray())
                }
            }
        }

        return true
    }

    private fun sendHelpMessage() {
        "${yellow}${bold}${underline}Available commands${yellow}${bold}:".addCLTPrefix().toChatComponent().addChatMessage()
        commands.forEach { commandAbstract ->
            val prefix = COMMAND_PREFIX[0]
            val alias = commandAbstract.aliases[0]
            if (!commandAbstract::class.java.annotations.map { it::class.java }.contains(HideCommand::class.java)) {
                " $grey$bold$rightArrow$reset $dark_aqua/$prefix $alias".addCLTPrefix().toChatComponent().addChatMessage()
            }
        }
    }

    fun init() {
        addCommands()
        EventBus.subscribe(ChatSentEvent::class.java, chatConsumer)
    }

    private fun addCommands() {
        arrayOf(
            DebugCommand,
            BedwarsCommand,
        ).forEach(::registerCommand)
    }

    private fun registerCommand(command: CommandAbstract) {
        commands.add(command)
    }
}