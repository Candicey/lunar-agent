@file:Suppress("unused")

package com.gitlab.candicey.celestial.task

import club.maxstats.weave.loader.api.event.ChatReceivedEvent
import com.gitlab.candicey.celestial.extension.*
import com.gitlab.candicey.celestial.helper.message.MessageHelper
import com.gitlab.candicey.celestial.util.TimeCountUtil
import com.gitlab.candicey.celestial.util.red
import java.util.concurrent.Executors
import java.util.function.Consumer
import kotlin.properties.Delegates

object HypixelTask {
    private val executors = Executors.newFixedThreadPool(30)

    @JvmStatic
    fun api(callBack: (String) -> Unit, noResponseCallBack: (() -> Unit)? = ::sendDidNotReceiveResponse) {
        val timeCountUtil = TimeCountUtil.newInstance(5)
        var consumer: Consumer<ChatReceivedEvent> by Delegates.notNull()
        consumer = Consumer { event ->
            val message = event.message.formattedText
            if (timeCountUtil.isTimeOver()) {
                noResponseCallBack?.invoke()
                MessageHelper.removeS2C(consumer)
            } else {
                val prefix = "§aYour new API key is §r§b"
                if (message.startsWith(prefix)) {
                    val apiKey = message
                        .split(prefix.toRegex(), 2)[1]
                        .clearedFormattingText()
                        .trim()

                    executors.submit {
                        callBack(apiKey)
                    }

                    event.cancelled = true
                    MessageHelper.removeS2C(consumer)
                }
            }
        }
        MessageHelper.addS2C(consumer)
        "/api new".toChatComponent().sendChatMessage()
    }

    @JvmStatic
    fun list(callBack: (List<String>) -> Unit, noResponseCallBack: (() -> Unit)? = ::sendDidNotReceiveResponse) {
        val timeCountUtil = TimeCountUtil.newInstance(5)
        var consumer: Consumer<ChatReceivedEvent> by Delegates.notNull()
        consumer = Consumer { event ->
            if (timeCountUtil.isTimeOver()) {
                MessageHelper.removeS2C(consumer)
                noResponseCallBack?.invoke()
            } else {
                val message = event.message.formattedText.trim()
                if (message.startsWith("§rOnline Players (")) {
                    val playerList = event.message.unformattedText
                        .clearedFormattingText()
                        .split("§rOnline Players \\((\\d+)\\): ".toRegex(), 2)[1]
                        .split(",")
                        .map { it.delete("\\[.*]".toRegex()).trim() }

                    executors.submit {
                        callBack(playerList)
                    }

                    event.cancelled = true
                    MessageHelper.removeS2C(consumer)
                }
            }
        }
        MessageHelper.addS2C(consumer)
        "/list".toChatComponent().sendChatMessage()
    }

    @JvmStatic
    fun who(callBack: (List<String>) -> Unit, noResponseCallBack: (() -> Unit)? = ::sendDidNotReceiveResponse) {
        val timeCountUtil = TimeCountUtil.newInstance(5)
        var consumer: Consumer<ChatReceivedEvent> by Delegates.notNull()
        consumer = Consumer { event ->
            val message = event.message.formattedText
            if (timeCountUtil.isTimeOver()) {
                noResponseCallBack?.invoke()
                MessageHelper.removeS2C(consumer)
            } else {
                if (message.startsWith("§r§b§lONLINE: ")) {
                    val playerList = event.message.unformattedText
                        .clearedFormattingText()
                        .split("ONLINE: ".toRegex(), 2)[1]
                        .split(",")
                        .map { it.delete("\\[.*]".toRegex()).trim() }

                    executors.submit {
                        callBack(playerList)
                    }

                    event.cancelled = true
                    MessageHelper.removeS2C(consumer)
                }
            }
        }
        MessageHelper.addS2C(consumer)
        "/who".toChatComponent().sendChatMessage()
    }

    @JvmStatic
    fun party(callBack: (PartyPlayers) -> Unit, noResponseCallBack: (() -> Unit)? = ::sendDidNotReceiveResponse) {
        val customPartyRunnable = CustomPartyRunnable(callBack, noResponseCallBack)
        executors.submit(customPartyRunnable)
    }

    @JvmStatic
    fun sendDidNotReceiveResponse() {
        "${red}Did not receive any response from the server.".addCLTPrefix().toChatComponent().addChatMessage()
    }

    data class PartyPlayers(
        var isInParty: Boolean = false,
        var players: MutableList<String> = ArrayList(),
    )

    private class CustomPartyRunnable(val callBack: (PartyPlayers) -> Unit, val noResponseCallBack: (() -> Unit)?) : Runnable {
        val lines = ArrayList<ChatReceivedEvent>()
        var consumer: Consumer<ChatReceivedEvent> by Delegates.notNull()

        var inProgress = false

        val blueLines = "§9§m-----------------------------------------------------§r"

        override fun run() {
            setConsumer()
            MessageHelper.addS2C(consumer)
            "/p list".toChatComponent().sendChatMessage()
        }

        fun runAfter() {
            val partyPlayers = PartyPlayers()

            for (line in lines) {
                if (line.message.formattedText.trim() == "§cYou are not currently in a party.§r") {
                    callBack(partyPlayers)
                    return
                }

                val plainMessage = line.message.unformattedText.clearedFormattingText().trim()
                if (!plainMessage.contains(": ")) {
                    continue
                }

                (plainMessage.split(": ".toRegex(), 2)[1].trim())
                    .deleteHypixelRanksAndOnlineOffline()
                    .trim()
                    .split("\\s+".toRegex())
                    .map { it.deleteHypixelRanksAndOnlineOffline().trim() }
                    .filter { it.isNotEmpty() }
                    .forEach {
                        partyPlayers.players.add(it)
                    }


            }

            if (partyPlayers.players.size != 0) {
                partyPlayers.isInParty = true
            }

            callBack(partyPlayers)
        }

        fun setConsumer() {
            consumer = Consumer {
                val timeCountUtil = TimeCountUtil.newInstance(5)
                if (timeCountUtil.isTimeOver()) {
                    noResponseCallBack?.invoke()
                    MessageHelper.removeS2C(consumer)
                } else if (!inProgress && lines.size == 0 && it.message.formattedText == blueLines) {
                    inProgress = true
                    lines.add(it)
                    it.cancelled = true
                } else if (inProgress) {
                    lines.add(it)
                    it.cancelled = true
                    if (inProgress && it.message.formattedText == blueLines) {
                        Thread {
                            runAfter()
                        }.start()
                        MessageHelper.removeS2C(consumer)
                    }
                }
            }
        }
    }
}