package com.gitlab.candicey.celestial

import club.maxstats.weave.loader.WeaveLoader
import club.maxstats.weave.loader.api.Hook
import club.maxstats.weave.loader.api.ModInitializer
import club.maxstats.weave.loader.api.event.EventBus
import com.gitlab.candicey.celestial.command.CommandManager
import com.gitlab.candicey.celestial.config.ConfigManager
import com.gitlab.candicey.celestial.extension.hookManager
import com.gitlab.candicey.celestial.helper.BedwarsStatsHelper
import com.gitlab.candicey.celestial.helper.message.MessageHelper
import com.gitlab.candicey.celestial.hook.GameSettingsHook
import com.gitlab.candicey.celestial.hook.LocaleHook
import com.gitlab.candicey.celestial.keybind.KeyBindManager
import com.gitlab.candicey.celestial.mod.ChatStatsChecker
import com.gitlab.candicey.celestial.util.BotUtil
import com.gitlab.candicey.celestial.util.ScaledResolutionUtil
import com.gitlab.candicey.celestial.util.runAsync
import org.objectweb.asm.tree.ClassNode

@Suppress("unused")
class CelestialMain : ModInitializer, Hook("net/minecraft/client/Minecraft") {
    private fun preInit() {
        info("Celestial is loading...")

        info("Initialising Config...")
        ConfigManager.init()
        info("Config initialised!")

        info("Registering hooks...")
        WeaveLoader.hookManager.register(GameSettingsHook)
        WeaveLoader.hookManager.register(LocaleHook)
//        hookManager.register(IChatComponentHook)
        info("Hooks registered!")

        info("Subscribing events...")
        EventBus.subscribe(KeyBindManager)
        EventBus.subscribe(BotUtil)
        EventBus.subscribe(ChatStatsChecker)
        EventBus.subscribe(MessageHelper)
        EventBus.subscribe(ScaledResolutionUtil)
        info("Events subscribed!")

        info("Initialising Commands...")
        CommandManager.init()
        info("Commands initialised!")

        runAsync(BedwarsStatsHelper::check)

        info("Celestial loaded!")
    }

    override fun init() {
    }

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        preInit()
    }
}