package com.gitlab.candicey.celestial.helper

import com.gitlab.candicey.celestial.api.hypixel.HypixelApi
import com.gitlab.candicey.celestial.api.playerdb.PlayerDBApi
import com.gitlab.candicey.celestial.config.ConfigManager
import com.gitlab.candicey.celestial.extension.addCLTPrefix
import com.gitlab.candicey.celestial.extension.addChatMessage
import com.gitlab.candicey.celestial.extension.toChatComponent
import com.gitlab.candicey.celestial.hypixelApi
import com.gitlab.candicey.celestial.util.FormatUtil
import com.gitlab.candicey.celestial.util.dark_aqua
import com.gitlab.candicey.celestial.util.red
import com.gitlab.candicey.celestial.util.yellow

object BedwarsStatsHelper {
    var invalidHypixelApiKey = false

    fun check() {
        val apiKey = ConfigManager.getConfigApiData().hypixelApiKey

        val hypixelApiRegex = Regex("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}")
        if (!hypixelApiRegex.matches(apiKey)) {
            invalidHypixelApiKey = true
            return
        }

        hypixelApi = HypixelApi(apiKey)
        val validateApiKey = hypixelApi.validateApiKey()

        invalidHypixelApiKey = !validateApiKey.success
    }

    fun checkPlayerStats(username: String) {
        checkPlayerStats(username,
            { playerData, stats -> FormatUtil.formatStats(playerData, stats).addCLTPrefix().toChatComponent().addChatMessage() },
            { nick ->  "$yellow[???] $dark_aqua$nick$yellow is nicked!".addCLTPrefix().toChatComponent().addChatMessage() } )
    }

    fun checkPlayerStats(username: String, isRealPlayer: ((PlayerDBApi.PlayerDB, HypixelApi.HypixelStats) -> Unit)?, isNick: ((String) -> Unit)?) {
        val playerData = PlayerDBApi.getPlayerData(username)
        if (!playerData.success) {
            isNick?.invoke(username)
            return
        }

        val stats = hypixelApi.getStats(playerData.data.player.raw_id)
        if (!playerData.data.player.username.equals(stats.player?.displayname, true) || !username.equals(stats.player?.displayname, true)) {
            isNick?.invoke(username)
            return
        }

        isRealPlayer?.invoke(playerData, stats)
    }

    fun printInvalidApiKeyMessage() {
        "${red}Invalid Hypixel API Key!".addCLTPrefix().toChatComponent().addChatMessage()
    }
}