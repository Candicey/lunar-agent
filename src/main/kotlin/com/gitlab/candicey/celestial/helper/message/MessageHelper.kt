package com.gitlab.candicey.celestial.helper.message

import club.maxstats.weave.loader.api.event.ChatReceivedEvent
import club.maxstats.weave.loader.api.event.ChatSentEvent
import club.maxstats.weave.loader.api.event.SubscribeEvent
import java.util.function.Consumer

@Suppress("unused")
object MessageHelper {
    private var c2sConsumer: MutableList<Consumer<ChatSentEvent>> = mutableListOf()
        @Synchronized get
        @Synchronized set
    private var s2cConsumer: MutableList<Consumer<ChatReceivedEvent>> = mutableListOf()
        @Synchronized get
        @Synchronized set

    fun addC2S(consumer: Consumer<ChatSentEvent>) {
        c2sConsumer.add(consumer)
    }

    fun addS2C(consumer: Consumer<ChatReceivedEvent>) {
        s2cConsumer.add(consumer)
    }

    fun removeC2S(consumer: Consumer<ChatSentEvent>) {
        c2sConsumer.remove(consumer)
    }

    fun removeS2C(consumer: Consumer<ChatReceivedEvent>) {
        s2cConsumer.remove(consumer)
    }

    @SubscribeEvent
    fun c2s(message: ChatSentEvent) = c2sConsumer.toMutableList().forEach { it.accept(message) }

    @SubscribeEvent
    fun s2c(message: ChatReceivedEvent) = s2cConsumer.toMutableList().forEach { it.accept(message) }
}