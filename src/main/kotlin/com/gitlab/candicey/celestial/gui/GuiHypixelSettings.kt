package com.gitlab.candicey.celestial.gui

import com.gitlab.candicey.celestial.child.GuiTextFieldChild
import com.gitlab.candicey.celestial.config.ConfigManager
import com.gitlab.candicey.celestial.extension.addButton
import com.gitlab.candicey.celestial.extension.addCLTPrefix
import com.gitlab.candicey.celestial.extension.addChatMessage
import com.gitlab.candicey.celestial.extension.toChatComponent
import com.gitlab.candicey.celestial.helper.BedwarsStatsHelper
import com.gitlab.candicey.celestial.util.lime
import com.gitlab.candicey.celestial.util.red
import com.gitlab.candicey.celestial.util.runAsync
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiScreen
import net.minecraft.client.gui.GuiTextField
import org.lwjgl.input.Keyboard

class GuiHypixelSettings : GuiScreen() {
    private lateinit var hypixelApiKeyTextField: GuiTextField

    override fun initGui() {
        hypixelApiKeyTextField = GuiTextFieldChild(
            501,
            fontRendererObj,
            width / 2 - 100,
            35,
            200,
            20,
        ).apply { maxStringLength = 36 }

        hypixelApiKeyTextField.text = ConfigManager.getConfigApiData().hypixelApiKey

        addButton(
            GuiButton(
                401,
                width / 2 - 100,
                70,
                200,
                20,
                "Chat Stats Checker: ${if (ConfigManager.getConfigHypixelData().chatStatsCheckerJson.enabled) "${lime}enabled" else "${red}disabled"}"
            )
        )
    }

    override fun drawScreen(p0: Int, p1: Int, p2: Float) {
        drawRect(0, 0, width, height, 0xAA000000.toInt())
        super.drawScreen(p0, p1, p2)
        drawCenteredString(fontRendererObj, "Hypixel API Key", width / 2, 20, 0xFFFFFF)
        hypixelApiKeyTextField.drawTextBox()
    }

    override fun actionPerformed(button: GuiButton?) {
        if (button == null) {
            return
        }

        when (button.id) {
            401 -> {
                ConfigManager.getConfigHypixelData().chatStatsCheckerJson.enabled = !ConfigManager.getConfigHypixelData().chatStatsCheckerJson.enabled
                button.displayString = "Chat Stats Checker: ${if (ConfigManager.getConfigHypixelData().chatStatsCheckerJson.enabled) "${lime}enabled" else "${red}disabled"}"
            }
        }
    }

    override fun mouseClicked(p0: Int, p1: Int, p2: Int) {
        hypixelApiKeyTextField.mouseClicked(p0, p1, p2)

        buttonList.forEach {
            if (it.mousePressed(mc, p0, p1)) {
                actionPerformed(it)
            }
        }
    }

    override fun keyTyped(char: Char, keyCode: Int) {
        if (keyCode == Keyboard.KEY_ESCAPE) {
            mc.displayGuiScreen(null)
            return
        }

        if (hypixelApiKeyTextField.isFocused) {
            hypixelApiKeyTextField.textboxKeyTyped(char, keyCode)
        }
    }

    override fun onGuiClosed() {
        hypixelApiKeyTextField.text = hypixelApiKeyTextField.text.trim()

        val configApiData = ConfigManager.getConfigApiData()
        if (configApiData.hypixelApiKey != hypixelApiKeyTextField.text) {
            configApiData.hypixelApiKey = hypixelApiKeyTextField.text
            runAsync {
                "Checking Hypixel API key...".addCLTPrefix().toChatComponent().addChatMessage()
                BedwarsStatsHelper.check()
                if (BedwarsStatsHelper.invalidHypixelApiKey) {
                    BedwarsStatsHelper.printInvalidApiKeyMessage()
                } else {
                    "${lime}Hypixel API key is valid.".addCLTPrefix().toChatComponent().addChatMessage()
                }
            }
        }

        ConfigManager.save()
    }
}