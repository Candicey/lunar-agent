package com.gitlab.candicey.celestial.extension

import com.gitlab.candicey.celestial.util.grey
import com.gitlab.candicey.celestial.util.pink
import com.gitlab.candicey.celestial.util.yellow
import net.minecraft.util.ChatComponentText
import net.minecraft.util.IChatComponent
import java.net.HttpURLConnection
import java.net.URL

fun String.delete(regex: Regex): String = replace(regex, "")

fun String.clearedFormattingText(): String = delete(Regex("§[0-9a-fk-or]"))

fun String.toHttpUrlConnection(): HttpURLConnection = URL(this).openConnection() as HttpURLConnection

fun String.addCLTPrefix(): String = "$grey[${pink}CLT$grey]$yellow $this"

fun String.toChatComponent(): IChatComponent = ChatComponentText(this)

fun String.isValidMinecraftUsername(): Boolean = Regex("^[a-zA-Z0-9_]{1,16}$").matches(this)

fun String.deleteHypixelRanksAndOnlineOffline(): String = delete(Regex("§[0-9a-fk-or]")).delete(Regex("\\[.*]")).delete(Regex("\\(.*\\)")).delete("●".toRegex()).delete(" {2}".toRegex())