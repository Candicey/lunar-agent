package com.gitlab.candicey.celestial.extension

import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode

fun ClassNode.addField(
    access: Int,
    name: String,
    descriptor: String,
    signature: String?,
    value: Any?
) {
    fields.add(FieldNode(access, name, descriptor, signature, value))
}