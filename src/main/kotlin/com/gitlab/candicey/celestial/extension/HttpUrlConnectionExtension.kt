package com.gitlab.candicey.celestial.extension

import com.gitlab.candicey.celestial.GSON
import java.net.HttpURLConnection

fun HttpURLConnection.setData() = apply {
    requestMethod = "GET"
    connectTimeout = 5000
    readTimeout = 5000
}

fun HttpURLConnection.getText(): String = (if (responseCode in 200..299) inputStream else errorStream).bufferedReader().readText()

inline fun <reified T> HttpURLConnection.getJson(): T = GSON.fromJson(getText(), T::class.java)