package com.gitlab.candicey.celestial.extension

import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiScreen

fun GuiScreen.addButton(vararg guiButton: GuiButton) = run {
    val field = GuiScreen::class.java.getDeclaredField("buttonList")
    field.isAccessible = true
    field.set(this, field.get(this) as MutableList<*> + guiButton.asList())
}