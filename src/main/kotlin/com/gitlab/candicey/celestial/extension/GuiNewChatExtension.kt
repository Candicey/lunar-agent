package com.gitlab.candicey.celestial.extension

import net.minecraft.client.gui.ChatLine
import net.minecraft.client.gui.GuiNewChat
import net.minecraft.util.IChatComponent

private val chatLinesField = GuiNewChat::class.java.getDeclaredField("chatLines").apply { isAccessible = true }
private val drawnChatLinesField = GuiNewChat::class.java.getDeclaredField("drawnChatLines").apply { isAccessible = true }

private val setChatLineMethod = GuiNewChat::class.java.getDeclaredMethod("setChatLine", IChatComponent::class.java, Int::class.java, Int::class.java, Boolean::class.java).apply { isAccessible = true }

@Suppress("UNCHECKED_CAST")
val GuiNewChat.chatLines: MutableList<ChatLine>
    get() = chatLinesField.get(this) as MutableList<ChatLine>
@Suppress("UNCHECKED_CAST")
val GuiNewChat.drawnChatLines: MutableList<ChatLine>
    get() = drawnChatLinesField.get(this) as MutableList<ChatLine>

fun GuiNewChat.setChatLine(chatComponent: IChatComponent, chatLineId: Int, updateCounter: Int, displayOnly: Boolean) = setChatLineMethod.invoke(this, chatComponent, chatLineId, updateCounter, displayOnly)