package com.gitlab.candicey.celestial.extension

import club.maxstats.weave.loader.WeaveLoader
import club.maxstats.weave.loader.api.HookManager

private val hookManagerField =
    WeaveLoader::class.java.getDeclaredField("hookManager").apply { isAccessible = true }
val WeaveLoader.hookManager: HookManager
    get() = hookManagerField.get(this) as HookManager