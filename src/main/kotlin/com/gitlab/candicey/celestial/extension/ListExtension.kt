package com.gitlab.candicey.celestial.extension

import org.objectweb.asm.tree.MethodNode

fun List<MethodNode>.named(name: String) = find { it.name == name }!!