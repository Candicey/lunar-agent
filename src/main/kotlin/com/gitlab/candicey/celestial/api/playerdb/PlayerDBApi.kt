package com.gitlab.candicey.celestial.api.playerdb

import com.gitlab.candicey.celestial.PLAYERDB
import com.gitlab.candicey.celestial.extension.getJson
import com.gitlab.candicey.celestial.extension.setData
import com.gitlab.candicey.celestial.extension.toHttpUrlConnection
import com.gitlab.candicey.celestial.util.CacheUtil

object PlayerDBApi {
    private val playerDataCache = CacheUtil<String, PlayerDB>()

    fun getPlayerData(username: String): PlayerDB = playerDataCache.getOrPut(username) {
        "$PLAYERDB/$username".toHttpUrlConnection().setData().getJson()
    }

    @Suppress("PropertyName")
    data class PlayerDB(
        val success: Boolean,
        val code: String,
        val message: String,
        val data: PlayerData,
    ) {
        data class PlayerData(
            val player: Player,
        ) {
            data class Player(
                val meta: Meta,
                val username: String,
                val id: String,
                val raw_id: String,
                val avatar: String,
                val name_history: List<String>,
            ) {
                data class Meta(
                    val cached_at: Long,
                )
            }
        }
    }
}