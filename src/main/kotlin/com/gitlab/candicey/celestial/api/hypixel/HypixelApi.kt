package com.gitlab.candicey.celestial.api.hypixel

import com.gitlab.candicey.celestial.HYPIXEL
import com.gitlab.candicey.celestial.extension.getJson
import com.gitlab.candicey.celestial.extension.setData
import com.gitlab.candicey.celestial.extension.toHttpUrlConnection
import com.gitlab.candicey.celestial.util.CacheUtil
import com.google.gson.annotations.SerializedName

class HypixelApi(@Suppress("MemberVisibilityCanBePrivate") val apiKey: String) {
    private val hypixelStatsCache = CacheUtil<String, HypixelStats>()

    fun validateApiKey(): HypixelKey = "$HYPIXEL/key?key=${apiKey}".toHttpUrlConnection().setData().getJson()

    fun getStats(uuid: String): HypixelStats = hypixelStatsCache.getOrPut(uuid) {
        "$HYPIXEL/player?key=${apiKey}&uuid=${uuid}".toHttpUrlConnection().setData().getJson()
    }

    data class HypixelKey(
        val success: Boolean,
        val record: HypixelKeyRecord,
        val cause: String? = null
    ) {
        data class HypixelKeyRecord(
            val key: String,
            val owner: String,
            val limit: Int,
            val queriesInPastMin: Int,
            val totalQueries: Int
        )
    }

    @Suppress("SpellCheckingInspection", "PropertyName")
    data class HypixelStats(
        val success: Boolean,
        val player: Player? = null,
    ) {
        data class Player(
            @SerializedName("_id")
            var id: String = "",
            var uuid: String = "",
            var displayname: String = "",
            var stats: Stats? = null,
            var achievements: Achievement? = null,
        ) {
            data class Stats(
                @SerializedName("Bedwars")
                var bedwars: Bedwars? = null,
            ) {
                data class Bedwars(
                    var final_kills_bedwars: String? = null,
                    var final_deaths_bedwars: String? = null,
                    var wins_bedwars: String? = null,
                    var losses_bedwars: String? = null,
                    var winstreak: String? = null,
                )
            }

            data class Achievement(
                var bedwars_level: String? = null,
            )
        }
    }
}