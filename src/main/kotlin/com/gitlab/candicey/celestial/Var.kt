@file:Suppress("unused")

package com.gitlab.candicey.celestial

import com.gitlab.candicey.celestial.api.hypixel.HypixelApi
import com.gitlab.candicey.celestial.util.ScaledResolutionUtil
import com.google.gson.Gson
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.FontRenderer
import net.minecraft.entity.player.EntityPlayer
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

val LOGGER: Logger = LogManager.getLogger("Celestial")

const val PREFIX = "/"
val COMMAND_PREFIX = arrayOf("celestial", "clt")

val GSON: Gson = Gson()
val GSON_PRETTY: Gson = Gson().newBuilder().setPrettyPrinting().create()

const val HYPIXEL: String = "https://api.hypixel.net"
const val PLAYERDB: String = "https://playerdb.co/api/player/minecraft"

val mc: Minecraft
    get() = Minecraft.getMinecraft()
val fontRenderer: FontRenderer
    get() = mc.fontRendererObj
val displayWidth: Int
    get() = mc.displayWidth
val displayHeight: Int
    get() = mc.displayHeight
val gameWidth: Int
    get() = ScaledResolutionUtil.scaledResolution.scaledWidth
val gameHeight: Int
    get() = ScaledResolutionUtil.scaledResolution.scaledHeight

lateinit var hypixelApi: HypixelApi

val botList = mutableListOf<EntityPlayer>()