package com.gitlab.candicey.celestial.mod

import club.maxstats.weave.loader.api.event.ChatReceivedEvent
import club.maxstats.weave.loader.api.event.SubscribeEvent
import com.gitlab.candicey.celestial.command.impl.BedwarsCommand
import com.gitlab.candicey.celestial.config.ConfigManager
import com.gitlab.candicey.celestial.extension.*
import com.gitlab.candicey.celestial.helper.BedwarsStatsHelper
import com.gitlab.candicey.celestial.mc
import com.gitlab.candicey.celestial.util.*
import net.minecraft.client.gui.ChatLine
import net.minecraft.event.ClickEvent
import net.minecraft.event.HoverEvent
import net.minecraft.util.IChatComponent

object ChatStatsChecker {
    /*
0            [MVP++] NAME
1            Hypixel Level: 99
2            Achievement Points: 5,000
3            Guild: §bGUILD
4
5            Click to view NAME\u0027s profile!
    */
    private val hoverRegex =
        Regex("(.*)\nHypixel Level: \\d+\nAchievement Points: \\d+(?:,\\d+)*\nGuild: (.*)\n\nClick to view (.*)'s profile!")

    @SubscribeEvent
    fun onChatReceived(event: ChatReceivedEvent) {
        if (!ConfigManager.getConfigHypixelData().chatStatsCheckerJson.enabled) {
            return
        }

        val (valid, username, iChatComponent) = check(event.message)
        if (valid) {
            if (BedwarsStatsHelper.invalidHypixelApiKey) {
                val originalMessage = iChatComponent.chatStyle.chatHoverEvent.value.formattedText
                    .split("\n")
                    .filter { it.clearedFormattingText().isNotEmpty() }
                    .toMutableList()

                val message =
                    "\n$red$bold[$white${bold}Celestial$red$bold] $reset${red}Invalid Hypixel API Key! Please set it in the LunarAgent config!$reset\n"
                originalMessage.add(4, message)

                val newMessage = originalMessage.joinToString("\n")
                iChatComponent.chatStyle.chatHoverEvent =
                    HoverEvent(HoverEvent.Action.SHOW_TEXT, newMessage.toChatComponent())

                return
            }

            BedwarsCommand.executors.submit {
                BedwarsStatsHelper.checkPlayerStats(username, { _, hypixelStats ->
                    val star = hypixelStats.player?.achievements?.bedwars_level ?: "???"
                    val starColour = FormatUtil.getStarColour(star)
                    val (fkdr, wlr, winstreak) = FormatUtil.getFkdrWinrateWinstreak(hypixelStats.player)

                    val originalMessage = iChatComponent.chatStyle.chatHoverEvent.value.formattedText
                        .split("\n")
                        .filter { it.clearedFormattingText().isNotEmpty() }
                        .toMutableList()

                    val message = "\n${grey}Bedwars Level: $starColour$star\n" +
                            "${grey}FKDR: $orange$fkdr\n" +
                            "${grey}WLR: $orange$wlr\n" +
                            "${grey}Winstreak: $orange$winstreak$reset\n"
                    originalMessage.add(4, message)

                    val newMessage = originalMessage.joinToString("\n")
                    iChatComponent.chatStyle.chatHoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, newMessage.toChatComponent())

                    val chatLines = mc.ingameGUI.chatGUI.chatLines
                    val find = chatLines.find { chatLine ->
                        chatLine.chatComponent.equalsOther(event.message)
                    }

                    if (find != null) {
                        val drawnChatLines = mc.ingameGUI.chatGUI.drawnChatLines
                        synchronized(chatLines) {
                            synchronized(drawnChatLines) {
                                drawnChatLines.clear()
                                for (i in chatLines.indices.reversed()) {
                                    val chatline: ChatLine = chatLines[i]
                                    mc.ingameGUI.chatGUI.setChatLine(
                                        chatline.chatComponent,
                                        chatline.chatLineID,
                                        chatline.updatedCounter,
                                        true
                                    )
                                }
                            }
                        }
                    }
                }, { _ ->
                    val originalMessage = iChatComponent.chatStyle.chatHoverEvent.value.formattedText
                        .split("\n")
                        .filter { it.clearedFormattingText().isNotEmpty() }
                        .toMutableList()

                    val message =
                        "\n$red$bold[$white${bold}Celestial$red$bold] $reset${red}Failed to get stats for $username!$reset\n"
                    originalMessage.add(4, message)

                    val newMessage = originalMessage.joinToString("\n")
                    iChatComponent.chatStyle.chatHoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, newMessage.toChatComponent())

                    val chatLines = mc.ingameGUI.chatGUI.chatLines
                    val find = chatLines.find { chatLine ->
                        chatLine.chatComponent.equalsOther(event.message)
                    }

                    if (find != null) {
                        val drawnChatLines = mc.ingameGUI.chatGUI.drawnChatLines
                        synchronized(chatLines) {
                            synchronized(drawnChatLines) {
                                drawnChatLines.clear()
                                for (i in chatLines.indices.reversed()) {
                                    val chatline: ChatLine = chatLines[i]
                                    mc.ingameGUI.chatGUI.setChatLine(
                                        chatline.chatComponent,
                                        chatline.chatLineID,
                                        chatline.updatedCounter,
                                        true
                                    )
                                }
                            }
                        }
                    }
                })
            }
        }
    }

    private fun check(iChatComponent: IChatComponent): Triple<Boolean, String, IChatComponent> {
        val chatStyle = iChatComponent.chatStyle
        if (chatStyle.chatHoverEvent != null
            && chatStyle.chatClickEvent != null
            && chatStyle.chatHoverEvent.action == HoverEvent.Action.SHOW_TEXT
            && chatStyle.chatClickEvent.action == ClickEvent.Action.RUN_COMMAND
        ) {
            val hoverText = chatStyle.chatHoverEvent.value.unformattedText.clearedFormattingText()
            val match = hoverRegex.find(hoverText)
            if (match != null && match.groupValues.size == 4) {
                return Triple(true, match.groupValues[3], iChatComponent)
            }
        }

        if (iChatComponent.siblings.isNotEmpty()) {
            iChatComponent.siblings
                .map(::check)
                .find { it.first }
                ?.let { return it }
        }

        return Triple(false, "", iChatComponent)
    }
}