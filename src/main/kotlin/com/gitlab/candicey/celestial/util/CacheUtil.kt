package com.gitlab.candicey.celestial.util

class CacheUtil<T, U>(
    val cacheTime: Long = 3 * 60 * 1000, // 3 minutes
    val maxCacheSize: Int = 300,
) {
    private val cache = object : LinkedHashMap<T, Pair<Long, U>>(maxCacheSize * 10 / 7, 0.7f, true) {
        override fun removeEldestEntry(eldest: MutableMap.MutableEntry<T, Pair<Long, U>>?): Boolean {
            return size > maxCacheSize
        }
    }

    fun store(key: T, value: U) {
        update()
        cache[key] = System.currentTimeMillis() to value
    }

    fun getOrPut(key: T, value: () -> U): U {
        update()
        return cache[key]?.second ?: value().also { store(key, it) }
    }

    private fun update() {
        cache.entries.removeIf { it.value.first + cacheTime < System.currentTimeMillis() }
    }
}