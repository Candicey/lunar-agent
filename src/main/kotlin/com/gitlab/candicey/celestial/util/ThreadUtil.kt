package com.gitlab.candicey.celestial.util

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future

private val executors: ExecutorService = Executors.newFixedThreadPool(50) ?: error("Can't create new ExecutorService")

fun runAsync(block: () -> Unit): Future<*> = executors.submit(block)