package com.gitlab.candicey.celestial.util

import club.maxstats.weave.loader.api.event.SubscribeEvent
import club.maxstats.weave.loader.api.event.TickEvent
import com.gitlab.candicey.celestial.mc
import net.minecraft.client.gui.ScaledResolution

object ScaledResolutionUtil {
    lateinit var scaledResolution: ScaledResolution

    @SubscribeEvent
    fun onTick(event: TickEvent) {
        scaledResolution = ScaledResolution(mc)
    }
}