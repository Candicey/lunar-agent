package com.gitlab.candicey.celestial.util

import club.maxstats.weave.loader.api.event.SubscribeEvent
import club.maxstats.weave.loader.api.event.TickEvent
import com.gitlab.candicey.celestial.botList
import com.gitlab.candicey.celestial.mc

object BotUtil {
    @SubscribeEvent
    fun onTick(event: TickEvent) {
        val tabPlayerList = PlayerUtil.getTabPlayerList()

        mc.theWorld?.playerEntities?.forEach { entityPlayer ->
            if (!botList.contains(entityPlayer) && !tabPlayerList.contains(entityPlayer)) {
                botList.add(entityPlayer)
            } else if (botList.contains(entityPlayer) && tabPlayerList.contains(entityPlayer)) {
                botList.remove(entityPlayer)
            }
        }
    }
}