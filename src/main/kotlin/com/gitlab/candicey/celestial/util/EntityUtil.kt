package com.gitlab.candicey.celestial.util

import com.gitlab.candicey.celestial.mc
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityLivingBase
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.EntitySelectors
import net.minecraft.util.MovingObjectPosition
import net.minecraft.util.Vec3


object EntityUtil {
    private var pointedEntity: Entity? = null

    /**
     * From ToroHealth Damage Indicators
     */
    fun getLookingEntity(reachDistance: Double = 50.0): EntityLivingBase? {
        val movingObjectPosition = getMouseOver(reachDistance)
        return if (movingObjectPosition != null && MovingObjectPosition.MovingObjectType.ENTITY == movingObjectPosition.typeOfHit && movingObjectPosition.entityHit is EntityLivingBase) {
            movingObjectPosition.entityHit as EntityLivingBase
        } else {
            null
        }
    }

    /**
     * From ToroHealth Damage Indicators
     */
    fun getMouseOver(reachDistance: Double = 50.0, partialTicks: Float = 1.0f): MovingObjectPosition? {
        val observer: Entity = mc.renderViewEntity ?: return null
        var objectMouseOver: MovingObjectPosition?
        if (mc.theWorld == null) {
            return null
        }
//        mc.pointedEntity = null
        objectMouseOver = observer.rayTrace(reachDistance, partialTicks)
        var d1 = reachDistance
        val vec3: Vec3 = observer.getPositionEyes(partialTicks)
        if (objectMouseOver != null) {
            d1 = objectMouseOver.hitVec.squareDistanceTo(vec3)
        }
        val vec31: Vec3 = observer.getLook(partialTicks)
        val vec32: Vec3 = vec3.addVector(
            vec31.xCoord * reachDistance,
            vec31.yCoord * reachDistance,
            vec31.zCoord * reachDistance
        )
        pointedEntity = null
        var vec33: Vec3? = null
        val f = 1.0f
        val list: List<*> = mc.theWorld.getEntitiesInAABBexcluding(
            observer,
            observer.entityBoundingBox.addCoord(
                vec31.xCoord * reachDistance,
                vec31.yCoord * reachDistance,
                vec31.zCoord * reachDistance
            ).expand(f.toDouble(), f.toDouble(), f.toDouble()),
            EntitySelectors.NOT_SPECTATING
        )
        var d2 = d1
        for (j in list.indices) {
            var d3: Double? = null
            val entity1 = list[j] as Entity
            val f1: Float = entity1.collisionBorderSize
            val axisalignedbb: AxisAlignedBB = entity1.entityBoundingBox.expand(f1.toDouble(), f1.toDouble(), f1.toDouble())
            val movingobjectposition: MovingObjectPosition? = axisalignedbb.calculateIntercept(vec3, vec32)
            if (axisalignedbb.isVecInside(vec3)) {
                if (d2 < 0.0) continue
                this.pointedEntity = entity1
                vec33 = if (movingobjectposition == null) vec3 else movingobjectposition.hitVec
                d2 = 0.0
                continue
            }
            if (movingobjectposition == null || vec3.distanceTo(movingobjectposition.hitVec)
                    .also { d3 = it } >= d2 && d2 != 0.0
            ) continue
            if (entity1 == observer.ridingEntity /*&& !observer.canRiderInteract()*/) {
                if (d2 != 0.0) continue
                this.pointedEntity = entity1
                vec33 = movingobjectposition.hitVec
                continue
            }
            this.pointedEntity = entity1
            vec33 = movingobjectposition.hitVec
            d3?.let { d2 = it }
        }
        if (this.pointedEntity != null && (d2 < d1 /*|| mc.objectMouseOver == null*/)) {
            objectMouseOver = MovingObjectPosition(this.pointedEntity, vec33)
            /*if (this.pointedEntity is EntityLivingBase || this.pointedEntity is EntityItemFrame) {
                mc.pointedEntity = this.pointedEntity
            }*/
        }
        return objectMouseOver
    }
}