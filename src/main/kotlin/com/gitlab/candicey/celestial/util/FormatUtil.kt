@file:Suppress("unused")

package com.gitlab.candicey.celestial.util

import com.gitlab.candicey.celestial.api.hypixel.HypixelApi
import com.gitlab.candicey.celestial.api.playerdb.PlayerDBApi

const val rightArrow = "»"

const val colourKey = "§"

const val reset = colourKey + "r"

const val grey = colourKey + "7"
const val pink = colourKey + "d"
const val yellow = colourKey + "e"
const val lime = colourKey + "a"
const val dark_aqua = colourKey + "3"
const val red = colourKey + "c"
const val orange = colourKey + "6"
const val white = colourKey + "f"

const val bold = colourKey + "l"
const val underline = colourKey + "n"

object FormatUtil {
    fun formatStats(playerData: PlayerDBApi.PlayerDB, hypixelStats: HypixelApi.HypixelStats): String {
        val star = hypixelStats.player?.achievements?.bedwars_level ?: "???"
        val colour = getStarColour(star)
        val name = playerData.data.player.username
        val (fkdr, wlr, winstreak) = getFkdrWinrateWinstreak(hypixelStats.player)
        return "$yellow[$star] $colour$name $yellow- $lime$fkdr$yellow FKDR - $lime$wlr$yellow winrate - $lime$winstreak$yellow WS"
    }

    fun getFkdrWinrateWinstreak(player: HypixelApi.HypixelStats.Player?): Triple<String, String, String> =
        if (player == null) {
            Triple("???", "???", "???")
        } else {
            val bedwars = player.stats?.bedwars
            if (bedwars == null) {
                Triple("???", "???", "???")
            } else {
                val fk = bedwars.final_kills_bedwars?.toFloat() ?: 0F
                val fd = bedwars.final_deaths_bedwars?.toFloat().run { if (this == 0F) 1F else this } ?: 1F
                val win = bedwars.wins_bedwars?.toFloat() ?: 0F
                val lose = bedwars.losses_bedwars?.toFloat().run { if (this == 0F) 1F else this } ?: 0F
                val winstreak = bedwars.winstreak ?: "???"
                Triple("%.2f".format(fk / fd), "%.2f".format(win / lose), winstreak)
            }
        }

    fun getStarColour(star: String): String =
        star.toIntOrNull().run {
            if (this == null) {
                grey
            } else if (this < 100) {
                dark_aqua
            } else if (this < 200) {
                orange
            } else {
                red
            }
        }
}