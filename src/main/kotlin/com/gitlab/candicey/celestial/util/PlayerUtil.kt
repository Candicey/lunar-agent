package com.gitlab.candicey.celestial.util

import com.gitlab.candicey.celestial.botList
import com.gitlab.candicey.celestial.mc
import com.gitlab.candicey.celestial.wrapper.GuiPlayerTabOverlayWrapper
import net.minecraft.entity.player.EntityPlayer

object PlayerUtil {
    fun getTabPlayerList(): List<EntityPlayer> =
        if (mc.thePlayer == null) {
            emptyList()
        } else {
            GuiPlayerTabOverlayWrapper.entityOrdering
                .sortedCopy(mc.thePlayer.sendQueue.playerInfoMap)
                .filterNotNull()
                .map { mc.theWorld.getPlayerEntityByUUID(it.gameProfile.id) }
        }

    fun getTabPlayerListExcludeBot(): List<EntityPlayer> =
        getTabPlayerList().filter { !botList.contains(it) }
}