@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package com.gitlab.candicey.celestial.util

class TimeCountUtil(val startTime: Long, val endTime: Long) {
    companion object {
        @JvmStatic
        fun newInstance(seconds: Int) = TimeCountUtil(System.currentTimeMillis(), System.currentTimeMillis() + seconds * 1000)
    }

    constructor(seconds: Int) : this(System.currentTimeMillis(), System.currentTimeMillis() + seconds * 1000)

    fun getTimeLeft() = endTime - System.currentTimeMillis()

    fun getTimePassed() = System.currentTimeMillis() - startTime

    fun getTimeLeftPercent() = getTimeLeft() / getTimePassed().toDouble()

    fun getTimePassedPercent() = getTimePassed() / endTime.toDouble()

    fun getDifference() = endTime - startTime

    fun isTimeLeft() = getTimeLeft() > 0

    fun isTimeOver() = !isTimeLeft()
}