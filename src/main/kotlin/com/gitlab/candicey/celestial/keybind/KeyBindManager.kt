package com.gitlab.candicey.celestial.keybind

import club.maxstats.weave.loader.api.event.KeyboardEvent
import club.maxstats.weave.loader.api.event.SubscribeEvent
import com.gitlab.candicey.celestial.command.impl.bedwars.BedwarsPartyCommand
import com.gitlab.candicey.celestial.command.impl.bedwars.BedwarsSpecifyCommand
import com.gitlab.candicey.celestial.command.impl.bedwars.BedwarsWhoCommand
import com.gitlab.candicey.celestial.extension.addCLTPrefix
import com.gitlab.candicey.celestial.extension.addChatMessage
import com.gitlab.candicey.celestial.extension.toChatComponent
import com.gitlab.candicey.celestial.gui.GuiHypixelSettings
import com.gitlab.candicey.celestial.mc
import com.gitlab.candicey.celestial.util.EntityUtil
import net.minecraft.client.settings.KeyBinding
import net.minecraft.entity.player.EntityPlayer
import org.lwjgl.input.Keyboard

object KeyBindManager {
    /**
     * @see [com.gitlab.candicey.celestial.hook.LocaleHook]
     */
    val keyBindings = arrayOf(
        // KeyBinding("key descriptor", key, "category")
        KeyBind("celestial.gui.hypixel.settings", Keyboard.KEY_EQUALS, "Celestial Settings") { _, _ -> mc.displayGuiScreen(GuiHypixelSettings()) },
        KeyBind("celestial.bedwars.pointedentity", Keyboard.KEY_NONE, "Bedwars Stats Checker") { _, _ -> EntityUtil.getLookingEntity(300.0).let { entityLivingBase -> if (entityLivingBase != null && entityLivingBase is EntityPlayer) BedwarsSpecifyCommand.execute(listOf(entityLivingBase.name), "", emptyArray()) else "No pointed entity!".addCLTPrefix().toChatComponent().addChatMessage() } },
        KeyBind("celestial.bedwars.who", Keyboard.KEY_NONE, "Bedwars Stats Checker") { _, _ -> BedwarsWhoCommand.execute(emptyList(), "", emptyArray()) },
        KeyBind("celestial.bedwars.party", Keyboard.KEY_NONE, "Bedwars Stats Checker") { _, _ -> BedwarsPartyCommand.execute(emptyList(), "", emptyArray()) },
//        KeyBind("celestial.bedwars.list", Keyboard.KEY_NONE, "Bedwars Stats Checker") { _, _ -> BedwarsListCommand.handle(emptyList()) },
    )

    @SubscribeEvent
    fun onKeyboardEvent(event: KeyboardEvent) {
        keyBindings
            .filterNot { it.onlyActivateWhenKeyPressed && !it.isPressed }
            .forEach { it.onEvent(it, event) }
    }
}

@Suppress("MemberVisibilityCanBePrivate")
class KeyBind(
    val name: String,
    @get:JvmName("getKeyBindCode")
    val keyCode: Int,
    val category: String,
    val onlyActivateWhenKeyPressed: Boolean = true,
    val onEvent: (KeyBind, KeyboardEvent) -> Unit,
) : KeyBinding(
    name,
    keyCode,
    category,
)