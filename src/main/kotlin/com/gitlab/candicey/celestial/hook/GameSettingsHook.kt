package com.gitlab.candicey.celestial.hook

import club.maxstats.weave.loader.api.Hook
import club.maxstats.weave.loader.api.util.asm
import com.gitlab.candicey.celestial.keybind.KeyBindManager
import net.minecraft.client.settings.GameSettings
import org.apache.commons.lang3.ArrayUtils
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldInsnNode

object GameSettingsHook : Hook("net/minecraft/client/settings/GameSettings") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods
            .filter { it.name == "<init>" }
            .map { it.instructions }
            .forEach { insnList ->
                insnList.insert(insnList.find { abstractInsnNode ->
                    if (abstractInsnNode.opcode != Opcodes.PUTFIELD) {
                        return@find false
                    } else {
                        val fieldInsnNode = abstractInsnNode as FieldInsnNode
                        return@find fieldInsnNode.owner == "net/minecraft/client/settings/GameSettings" && fieldInsnNode.name == "keyBindings" && fieldInsnNode.desc == "[Lnet/minecraft/client/settings/KeyBinding;"
                    }
                }, asm {
                    aload(0)
                    invokestatic(
                        "com/gitlab/candicey/celestial/hook/GameSettingsHook",
                        "onInit",
                        "(Lnet/minecraft/client/settings/GameSettings;)V"
                    )
                })
            }
    }

    @Suppress("unused")
    @JvmStatic
    fun onInit(gameSettings: GameSettings) {
        gameSettings.keyBindings = ArrayUtils.addAll(gameSettings.keyBindings, *KeyBindManager.keyBindings)
    }
}