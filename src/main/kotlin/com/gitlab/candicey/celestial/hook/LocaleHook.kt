package com.gitlab.candicey.celestial.hook

import club.maxstats.weave.loader.api.Hook
import club.maxstats.weave.loader.api.util.asm
import com.gitlab.candicey.celestial.extension.named
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.LabelNode

object LocaleHook : Hook("net/minecraft/client/resources/Locale") {
    /**
     * @see [com.gitlab.candicey.celestial.keybind.KeyBindManager]
     */
    private val messages = mapOf(
        "celestial.gui.hypixel.settings" to "Open Hypixel Settings",
        "celestial.bedwars.pointedentity" to "Pointed Player Stats",
        "celestial.bedwars.who" to "/who Players Stats",
        "celestial.bedwars.party" to "/p list Players Stats",
        "celestial.bedwars.list" to "/list Players Stats",
    )

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods.named("formatMessage").instructions.insert(asm {
            val label = LabelNode()
            aload(0)
            aload(1)
            invokestatic("com/gitlab/candicey/celestial/hook/LocaleHook", "getLunarAgentMessage", "(Ljava/lang/String;)Ljava/lang/String;")
            dup
            ifnull(label)
            areturn
            +label
            pop
        })
    }

    @Suppress("unused")
    @JvmStatic
    fun getLunarAgentMessage(string: String): String? = messages[string]
}