package com.gitlab.candicey.celestial.child

import net.minecraft.client.gui.FontRenderer
import net.minecraft.client.gui.GuiTextField

class GuiTextFieldChild(
    @get:JvmName("getIdChild")
    val id: Int,
    val fontRenderer: FontRenderer,
    val xPosition: Int,
    val yPosition: Int,
    @get:JvmName("getWidthChild")
    val width: Int,
    val height: Int,
) : GuiTextField(
    id,
    fontRenderer,
    xPosition,
    yPosition,
    width,
    height,
) {
    private val maxStringLengthField = javaClass.getField("maxStringLength").also { it.isAccessible = true }

    @get:JvmName("getMaxStringLengthChild")
    @set:JvmName("setMaxStringLengthChild")
    var maxStringLength: Int
        set(value) = maxStringLengthField.setInt(this, value)
        get() = maxStringLengthField.getInt(this)
}