package me.monmcgt.code.lunaragent.extension

import me.monmcgt.code.lunaragent.mc
import net.minecraft.util.IChatComponent

/*private val fromLunarAgentField = IChatComponent::class.java.getDeclaredField("fromLunarAgent")*/

/*var IChatComponent.fromLunarAgent: Boolean
    get() = fromLunarAgentField.getBoolean(this)
    set(value) = fromLunarAgentField.setBoolean(this, value)*/

fun IChatComponent.addChatMessage(/*addFromLA: Boolean = true*/) = mc.thePlayer.addChatMessage(this/*.also { it.fromLunarAgent = addFromLA }*/)

fun IChatComponent.sendChatMessage() = mc.thePlayer.sendChatMessage(this.unformattedText)

fun IChatComponent.equalsOther(other: IChatComponent): Boolean {
    if (siblings.size != other.siblings.size) {
        return false
    }

    if (siblings.isEmpty()) {
        if (unformattedText != other.unformattedText) {
            return false
        }

        if (chatStyle.chatHoverEvent.action != other.chatStyle.chatHoverEvent.action) {
            return false
        }

        if (chatStyle.chatHoverEvent.value.unformattedText != other.chatStyle.chatHoverEvent.value.unformattedText) {
            return false
        }

        if (chatStyle.chatClickEvent.action != other.chatStyle.chatClickEvent.action) {
            return false
        }

        if (chatStyle.chatClickEvent.value != other.chatStyle.chatClickEvent.value) {
            return false
        }

        return true
    } else {
        siblings.forEachIndexed { index, iChatComponent ->
            if (!iChatComponent.equals(other.siblings[index])) {
                return false
            }
        }

        return true
    }
}